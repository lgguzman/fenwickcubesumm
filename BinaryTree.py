class CubeBinaryTree:

    def __init__(self,n):
        self.binarytree=[[[0 for k in range(n+1)] for j in range(n+1)] for i in range(n+1)]
        self.lenght=n

    def update(self, x,y,z,val):
        while z <= self.lenght:
            xtemp = x
            while xtemp <= self.lenght:
                ytemp = y
                while ytemp <= self.lenght:
                    self.binarytree[xtemp][ytemp][z] += val
                    ytemp += ytemp & -ytemp
                xtemp += xtemp & -xtemp
            z += z & -z

    def sumToZeroAxis(self,x,y,z):
        sum=0
        while z > 0:
            xtemp=x
            while xtemp>0:
                ytemp=y
                while ytemp>0:
                    sum += self.binarytree[xtemp][ytemp][z]
                    ytemp -= ytemp & -ytemp
                xtemp -= xtemp & -xtemp
            z -= z & -z
        return sum
