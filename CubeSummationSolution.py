from BinaryTree import CubeBinaryTree


class CubeSummation:
    def __init__(self, n):
        self.cubeBinaryTree =  CubeBinaryTree(n)

    def query (self, x0,y0,z0,x,y,z):
        val1=self.cubeBinaryTree.sumToZeroAxis(x, y, z)-self.cubeBinaryTree.sumToZeroAxis(x0-1, y, z)-self.cubeBinaryTree.sumToZeroAxis(x, y0-1, z) + self.cubeBinaryTree.sumToZeroAxis(x0-1, y0-1, z)
        val2=self.cubeBinaryTree.sumToZeroAxis(x, y, z0-1)-self.cubeBinaryTree.sumToZeroAxis(x0-1, y, z0-1)-self.cubeBinaryTree.sumToZeroAxis(x, y0-1, z0-1) + self.cubeBinaryTree.sumToZeroAxis(x0-1, y0-1, z0-1)
        return val1-val2

    def update(self,x,y,z,val):
        val1 = self.cubeBinaryTree.sumToZeroAxis(x, y, z) - self.cubeBinaryTree.sumToZeroAxis(x - 1, y, z) - self.cubeBinaryTree.sumToZeroAxis(x, y - 1, z) + self.cubeBinaryTree.sumToZeroAxis(x - 1, y - 1, z)
        val2 = self.cubeBinaryTree.sumToZeroAxis(x, y, z - 1) - self.cubeBinaryTree.sumToZeroAxis(x - 1, y, z - 1) - self.cubeBinaryTree.sumToZeroAxis(x, y - 1, z - 1) + self.cubeBinaryTree.sumToZeroAxis(x - 1, y - 1, z - 1)
        self.cubeBinaryTree.update(x,y,z,val-val1+val2)

    def match(self):
        line = input()
        data = line.split()
        types = data[0]
        if types == "QUERY":
            print(self.query(int(data[1]), int(data[2]), int(data[3]), int(data[4]), int(data[5]), int(data[6])))
        else:
            self.update(int(data[1]), int(data[2]), int(data[3]), int(data[4]))



