from CubeSummationSolution import CubeSummation

cases = int(input())
while cases != 0:
    line = input()
    data = line.split()
    n = int(data[0])
    m = int(data[1])
    cubesumm = CubeSummation(n)
    while m != 0:
        cubesumm.match()
        m -= 1
    cases -= 1

